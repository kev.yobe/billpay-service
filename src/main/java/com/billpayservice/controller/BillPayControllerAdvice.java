package com.billpayservice.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


import com.billpayservice.exception.PaymentNotFoundException;
import com.billpayservice.exception.ReportNotFoundException;
import com.billpayservice.mapper.Mapper;

@ControllerAdvice
public class BillPayControllerAdvice {
	
	@Autowired
	Mapper mapper;
	
	@ExceptionHandler(PaymentNotFoundException.class)
	@ResponseBody
	public ResponseEntity<String> handlePaymentNotFoundException(PaymentNotFoundException ex ) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
		
	}
	
	@ExceptionHandler(ReportNotFoundException.class)
	@ResponseBody
	public ResponseEntity<String> handleReportNotFoundException(ReportNotFoundException ex) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
	}


}
