package com.billpayservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.billpayservice.dal.entity.PowertelReport;
import com.billpayservice.exception.ReportNotFoundException;
import com.billpayservice.service.ReportService;

@RequestMapping("report")
@RestController
public class ReportController {
	private ReportService reportService;

	@Autowired
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	@GetMapping("/reference/{participantReference}")
	public PowertelReport getByParticipantReference(@PathVariable("participantReference") String participantReference) {
		PowertelReport  report = reportService.getReportFromReference(participantReference);
		if (report == null) {
			throw new ReportNotFoundException(participantReference);
		}
		return report;
	}
}
