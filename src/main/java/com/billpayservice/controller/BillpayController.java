package com.billpayservice.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.billpayservice.dal.entity.Payment;
import com.billpayservice.dal.entity.PowertelReport;
import com.billpayservice.exception.PaymentNotFoundException;
import com.billpayservice.exception.ReportNotFoundException;
import com.billpayservice.mapper.Mapper;
import com.billpayservice.rest.model.PaymentResponse;
import com.billpayservice.rest.model.ReportResponse;
import com.billpayservice.service.PaymentService;
import com.billpayservice.service.ReportService;

@RestController
public class BillpayController {
	private PaymentService paymentService;
	private ReportService reportService;
	Mapper mapper;
	@Autowired
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}
	@Autowired
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}
	@Autowired
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	@GetMapping("/findbyid/{id}")
	public ResponseEntity<PaymentResponse> getPaymentById(@PathVariable("id") Long id) {
		Payment payment = paymentService.getPaymentbyId(id);
		if (payment != null) {
			PaymentResponse response = mapper.mapPayment(payment);
			return ResponseEntity.ok().body(response);
		}else {
			throw new PaymentNotFoundException(id);
		}
	}

	@GetMapping("/allpaymentsbyid/{id}")
	public  ResponseEntity<List<PaymentResponse>> getAllPaymentbyId(@PathVariable("id") List<Long> id) {
		List<PaymentResponse> responses = id.stream()
				.filter(e->paymentService.getPaymentbyId(e)!=null)
				.map(e->mapper.mapPayment(paymentService.getPaymentbyId(e)))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(responses);
	}

	@GetMapping("/allpaymentbyreferences/{references}")
	public ResponseEntity<List<PaymentResponse>>  getAllPaymentbyParticipantRef(@PathVariable("references") List<String> references) {
		List<PaymentResponse> responses = references.stream()
				.filter(e->paymentService.getPaymentByParticipantReference(e)!=null)
				.map(e->mapper.mapPayment(paymentService.getPaymentByParticipantReference(e)))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(responses);
	}

	@GetMapping("/allpayment/{reference}")
	public ResponseEntity<PaymentResponse> getPaymentbyParticipantRef(@PathVariable("reference") String reference) {
		Payment payment = paymentService.getPaymentByParticipantReference(reference);
		if (payment != null) {
			PaymentResponse response =   mapper.mapPayment(payment);
			return ResponseEntity.ok().body(response);
		}
		else {
			throw new PaymentNotFoundException(reference);
		}
	}

	@GetMapping("/getbybillaccount/{billAccount}")
	public ResponseEntity<List<PaymentResponse>> getPaymentByBillAccount(@PathVariable("billAccount") String billAccount) {

		List<Payment> payments = paymentService.findByBillAccount(billAccount);
		List<PaymentResponse> responses = payments.stream().filter(e->e!=null)
				.map(e->mapper.mapPayment(e))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(responses);
	}

	@GetMapping("/powertelReport/{participantReference}")
	public ResponseEntity<ReportResponse> getByVendorReference(@PathVariable("participantReference") String participantReference) {
		PowertelReport  report = reportService.getReportFromReference(participantReference);
		if (report != null) {
			ReportResponse response = mapper.mapReport(report);
			return ResponseEntity.ok().body(response);
		}
		else {
			throw new ReportNotFoundException(participantReference);
		}
		

	}

	@PostMapping("/save")
	public ResponseEntity<PowertelReport> saveReport(@RequestBody PowertelReport report) {
		PowertelReport rpt = reportService.saveReport(report);
		return ResponseEntity.ok().body(rpt);
	}

}
