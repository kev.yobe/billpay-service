package com.billpayservice.exception;

public class ReportNotFoundException extends RuntimeException{
	

	private static final long serialVersionUID = 1L;

	public ReportNotFoundException(String vendorReference) {
		super("Could not find powertel report with vendorRef: " + vendorReference);
	}

	public ReportNotFoundException(){
		super("Requested report was not found");
	}

}
