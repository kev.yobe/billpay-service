package com.billpayservice.exception;

public class PaymentNotFoundException extends RuntimeException{

	
	private static final long serialVersionUID = 1L;
	
	public PaymentNotFoundException(Long id) {
		super("Could not find payment with id " + id);
	}
	
	public PaymentNotFoundException(String reference) {
		super("Could not find payment with reference  " + reference);
	}

	public PaymentNotFoundException() {

	}
}
