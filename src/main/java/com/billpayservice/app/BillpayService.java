package com.billpayservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.billpayservice", "java.lang"})
@EnableJpaRepositories(basePackages = "com.billpayservice.dal.repo")
@EntityScan(basePackages = "com.billpayservice.dal.entity")
public class BillpayService {

	public static void main(String[] args) {
		SpringApplication.run(BillpayService.class, args);
	}

}
