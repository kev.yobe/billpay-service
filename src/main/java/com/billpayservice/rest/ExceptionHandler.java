package com.billpayservice.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.billpayservice.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.billpayservice.exception.PaymentNotFoundException;

public class ExceptionHandler implements ExceptionMapper<Exception> {


    Mapper mapper;
	@Autowired
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public Response toResponse(Exception exception) {
		
		if (exception instanceof PaymentNotFoundException) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mapper.mapPaymentException()).build();
		}
		
		else {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(mapper.mapReportException()).build();
		}
		
	}

}
