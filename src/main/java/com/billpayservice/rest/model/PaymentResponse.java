package com.billpayservice.rest.model;

import org.springframework.stereotype.Component;

@Component
public class PaymentResponse {

	private Long id;

	private String dateSubmitted;

	private String type;

	private String participantId;

	private String participantReference;

	private String provider;

	private String providerProduct;

	private String billAccount;

	private String billAmount;

	private String payerMsisdn;

	private String channel;

	private String state;

	private String success;

	private String portalResponse;

	private String portalDescription;

	private String providerResponse;

	private String providerDescription;

	private String providerReference;
	
	private String token;
	
	private String response;public PaymentResponse() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getParticipantId() {
		return participantId;
	}

	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}

	public String getParticipantReference() {
		return participantReference;
	}

	public void setParticipantReference(String participantReference) {
		this.participantReference = participantReference;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getProviderProduct() {
		return providerProduct;
	}

	public void setProviderProduct(String providerProduct) {
		this.providerProduct = providerProduct;
	}

	public String getBillAccount() {
		return billAccount;
	}

	public void setBillAccount(String billAccount) {
		this.billAccount = billAccount;
	}

	public String getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public String getPayerMsisdn() {
		return payerMsisdn;
	}

	public void setPayerMsisdn(String payerMsisdn) {
		this.payerMsisdn = payerMsisdn;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getPortalResponse() {
		return portalResponse;
	}

	public void setPortalResponse(String portalResponse) {
		this.portalResponse = portalResponse;
	}

	public String getPortalDescription() {
		return portalDescription;
	}

	public void setPortalDescription(String portalDescription) {
		this.portalDescription = portalDescription;
	}

	public String getProviderResponse() {
		return providerResponse;
	}

	public void setProviderResponse(String providerResponse) {
		this.providerResponse = providerResponse;
	}

	public String getProviderDescription() {
		return providerDescription;
	}

	public void setProviderDescription(String providerDescription) {
		this.providerDescription = providerDescription;
	}

	public String getProviderReference() {
		return providerReference;
	}

	public void setProviderReference(String providerReference) {
		this.providerReference = providerReference;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
