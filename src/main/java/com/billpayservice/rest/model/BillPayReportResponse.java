package com.billpayservice.rest.model;

import org.springframework.stereotype.Component;

@Component
public class BillPayReportResponse {
		
		
		private String vendorReference;
		
		private String dateTime;

		private String transactionReference;

		private String transactionType;

		private String token;

		private String narration;

		private String amount;

		private String balance;
		
		private String billAccount;

		public BillPayReportResponse() {
			
		}
		public String getDateTime() {
			return dateTime;
		}

		public void setDateTime(String dateTime) {
			this.dateTime = dateTime;
		}

		public String getVendorReference() {
			return vendorReference;
		}

		public void setVendorReference(String vendorReference) {
			this.vendorReference = vendorReference;
		}

		public String getTransactionReference() {
			return transactionReference;
		}

		public void setTransactionReference(String transactionReference) {
			this.transactionReference = transactionReference;
		}

		public String getTransactionType() {
			return transactionType;
		}

		public void setTransactionType(String transactionType) {
			this.transactionType = transactionType;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public String getNarration() {
			return narration;
		}

		public void setNarration(String narration) {
			this.narration = narration;
		}

		public String getAmount() {
			return amount;
		}

		public void setAmount(String amount) {
			this.amount = amount;
		}

		public String getBalance() {
			return balance;
		}

		public void setBalance(String balance) {
			this.balance = balance;
		}

		public String getBillAccount() {
			return billAccount;
		}

		public void setBillAccount(String billAccount) {
			this.billAccount = billAccount;
		}

	}

