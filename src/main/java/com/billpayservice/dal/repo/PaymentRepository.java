package com.billpayservice.dal.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.billpayservice.dal.entity.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
	public Optional<Payment> findById(Long id);
	public Optional<Payment> findOneByParticipantReferenceAndType(String participantReference, String type);
	public Optional<List<Payment>>findByBillAccount(String billAccount);
	
}
