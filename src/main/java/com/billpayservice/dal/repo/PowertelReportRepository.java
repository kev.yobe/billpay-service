package com.billpayservice.dal.repo;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.billpayservice.dal.entity.PowertelReport;

@Repository
public interface PowertelReportRepository extends JpaRepository<PowertelReport, Long>{
	public List<PowertelReport> findByVendorReference(String vendorReference);
	public Optional<PowertelReport> findById(Long id);

}
