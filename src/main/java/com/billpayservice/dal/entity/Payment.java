package com.billpayservice.dal.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Payments")
public class Payment {
	
	@Id
	private Long id;
	
	private String dateSubmitted;
	
	private String type;
	
	private String participantId;
	
	private String username;
	
	private String participantReference;
	
	private String additionalAuthReference;
	
	private String originalParticipantReference;
	
	private String participantPaymentDate;
	
	private String participantSettleDate;
	
	private String provider;
	
	private String providerProduct;
	
	private String billAccount;
	
	private String billAmount;
	
	private String payerName;
	
	private String payerReference;
	
	private String payerMsisdn;
	
	private String payerCardReference;
	
	private String payerAccountReference;
	
	private String paymentType;
	
	private String channel;
	
	private String additionalBillData;
	
	private String state;
	
	private String success;
	
	private String portalResponse;
	
	private String portalDescription;
	
	private String providerResponse;
	
	private String providerDescription;
	
	private String providerReference;
	
	private String receiptData;
	
	private String dateCompleted;
	
	private String linkedPaymentId;
	
	private String reversed;
	
	private String rspAdditionalBillData;
	
	private String lastUpdated;
	
	private String customProcessorId;
	
	public Payment() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getParticipantId() {
		return participantId;
	}

	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getParticipantReference() {
		return participantReference;
	}

	public void setParticipantReference(String participantReference) {
		this.participantReference = participantReference;
	}

	public String getAdditionalAuthReference() {
		return additionalAuthReference;
	}

	public void setAdditionalAuthReference(String additionalAuthReference) {
		this.additionalAuthReference = additionalAuthReference;
	}

	public String getOriginalParticipantReference() {
		return originalParticipantReference;
	}

	public void setOriginalParticipantReference(String originalParticipantReference) {
		this.originalParticipantReference = originalParticipantReference;
	}

	public String getParticipantPaymentDate() {
		return participantPaymentDate;
	}

	public void setParticipantPaymentDate(String participantPaymentDate) {
		this.participantPaymentDate = participantPaymentDate;
	}

	public String getParticipantSettleDate() {
		return participantSettleDate;
	}

	public void setParticipantSettleDate(String participantSettleDate) {
		this.participantSettleDate = participantSettleDate;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getProviderProduct() {
		return providerProduct;
	}

	public void setProviderProduct(String providerProduct) {
		this.providerProduct = providerProduct;
	}

	public String getBillAccount() {
		return billAccount;
	}

	public void setBillAccount(String billAccount) {
		this.billAccount = billAccount;
	}

	public String getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}

	public String getPayerName() {
		return payerName;
	}

	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}

	public String getPayerReference() {
		return payerReference;
	}

	public void setPayerReference(String payerReference) {
		this.payerReference = payerReference;
	}

	public String getPayerMsisdn() {
		return payerMsisdn;
	}

	public void setPayerMsisdn(String payerMsisdn) {
		this.payerMsisdn = payerMsisdn;
	}

	public String getPayerCardReference() {
		return payerCardReference;
	}

	public void setPayerCardReference(String payerCardReference) {
		this.payerCardReference = payerCardReference;
	}

	public String getPayerAccountReference() {
		return payerAccountReference;
	}

	public void setPayerAccountReference(String payerAccountReference) {
		this.payerAccountReference = payerAccountReference;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAdditionalBillData() {
		return additionalBillData;
	}

	public void setAdditionalBillData(String additionalBillData) {
		this.additionalBillData = additionalBillData;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getPortalResponse() {
		return portalResponse;
	}

	public void setPortalResponse(String portalResponse) {
		this.portalResponse = portalResponse;
	}

	public String getPortalDescription() {
		return portalDescription;
	}

	public void setPortalDescription(String portalDescription) {
		this.portalDescription = portalDescription;
	}

	public String getProviderResponse() {
		return providerResponse;
	}

	public void setProviderResponse(String providerResponse) {
		this.providerResponse = providerResponse;
	}

	public String getProviderDescription() {
		return providerDescription;
	}

	public void setProviderDescription(String providerDescription) {
		this.providerDescription = providerDescription;
	}

	public String getProviderReference() {
		return providerReference;
	}

	public void setProviderReference(String providerReference) {
		this.providerReference = providerReference;
	}

	public String getReceiptData() {
		return receiptData;
	}

	public void setReceiptData(String receiptData) {
		this.receiptData = receiptData;
	}

	public String getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(String dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public String getLinkedPaymentId() {
		return linkedPaymentId;
	}

	public void setLinkedPaymentId(String linkedPaymentId) {
		this.linkedPaymentId = linkedPaymentId;
	}

	public String getReversed() {
		return reversed;
	}

	public void setReversed(String reversed) {
		this.reversed = reversed;
	}

	public String getRspAdditionalBillData() {
		return rspAdditionalBillData;
	}

	public void setRspAdditionalBillData(String rspAdditionalBillData) {
		this.rspAdditionalBillData = rspAdditionalBillData;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCustomProcessorId() {
		return customProcessorId;
	}

	public void setCustomProcessorId(String customProcessorId) {
		this.customProcessorId = customProcessorId;
	}
}
