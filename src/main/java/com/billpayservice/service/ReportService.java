package com.billpayservice.service;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.billpayservice.dal.entity.Payment;
import com.billpayservice.dal.entity.PowertelReport;
import com.billpayservice.dal.repo.PowertelReportRepository;
import com.billpayservice.exception.PaymentNotFoundException;
import com.billpayservice.exception.ReportNotFoundException;

import org.springframework.dao.DataIntegrityViolationException;

@Component
public class ReportService {
	PowertelReportRepository reportRepository;
	PaymentService paymentService;
	@Autowired
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}
	@Autowired
	public void setReportRepository(PowertelReportRepository reportRepository) {
		this.reportRepository = reportRepository;
	}

	public PowertelReport getPaymentbyId(Long id) {
		Optional<PowertelReport> optional = reportRepository.findById(id);
		return optional.orElseThrow(ReportNotFoundException::new);
	}
	
	public PowertelReport findByVendorReference(String vendorReference) {
		String prefix = "000";
		if (!vendorReference.startsWith(prefix)) {
			vendorReference = prefix+vendorReference;
		}
		List<PowertelReport> report =  reportRepository.findByVendorReference(vendorReference);
		if (report.size() == 0) {
			throw new ReportNotFoundException(vendorReference);
		}
		return report.get(0);
	}
	
	public PowertelReport saveReport(PowertelReport report) {
		PowertelReport savedReport = reportRepository.save(report);
		return 	savedReport;
	}
	
	public PowertelReport getReportFromReference(String reference){
		Payment payment = paymentService.getPaymentByParticipantReference(reference);

		if (payment == null) {
			throw new PaymentNotFoundException(reference);
		}

		String vendorRef = "000" + String.valueOf(payment.getId());
		PowertelReport report = findByVendorReference(vendorRef);
		return report;
	}
	
	public Boolean checkReport(PowertelReport report) {
		boolean isAvailable = false;
		String token = report.getToken();
		if (token.isBlank()) {
			isAvailable = false;
		}
		else {
			isAvailable = true;
		}
		return isAvailable;
	}
	
	

}
