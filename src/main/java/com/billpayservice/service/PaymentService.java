package com.billpayservice.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.billpayservice.exception.PaymentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.billpayservice.dal.entity.Payment;
import com.billpayservice.dal.repo.PaymentRepository;


@Component
public class PaymentService {
	private PaymentRepository paymentRepository;
	private ReportService reportService;
	
	String type = "PAYMENT";
	@Autowired
	public void setPaymentRepository(PaymentRepository paymentRepository) {
		this.paymentRepository = paymentRepository;
	}

	@Autowired
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public Payment getPaymentByParticipantReference(String participantReference) {
		Optional<Payment> optional = paymentRepository.findOneByParticipantReferenceAndType(participantReference,type);
		return optional.orElseThrow(PaymentNotFoundException::new);
	}

	public Payment getPaymentbyId(Long id) {
		Optional<Payment> optional = paymentRepository.findById(id);
		return optional.orElseThrow(PaymentNotFoundException::new);
	}
	
	public List<Payment> findAllById(List<Long> id){
		return paymentRepository.findAllById(id);
		
	}
	
	public List<Payment> findByBillAccount(String billAccount){
		Optional<List<Payment>> optional = paymentRepository.findByBillAccount(billAccount);
		return optional.orElseThrow(PaymentNotFoundException::new);
	}

}
