package com.billpayservice.mapper;

import com.billpayservice.rest.model.PaymentResponse;
import com.billpayservice.rest.model.ReportResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.billpayservice.dal.entity.Payment;
import com.billpayservice.dal.entity.PowertelReport;
import com.billpayservice.service.PaymentService;
import com.billpayservice.service.ReportService;

@Component
public class Mapper {

	private ReportResponse reportResponse;

	private PaymentResponse paymentResponse;

	private ReportService reportService;

	private PaymentService paymentService;
	@Autowired
	public void setReportResponse(ReportResponse reportResponse) {
		this.reportResponse = reportResponse;
	}
	@Autowired
	public void setPaymentResponse(PaymentResponse paymentResponse) {
		this.paymentResponse = paymentResponse;
	}
	@Autowired
	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}
	@Autowired
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	public ReportResponse mapReport(PowertelReport report) {
		Long id = Long.parseLong(report.getVendorReference());
		Payment payment = paymentService.getPaymentbyId(id);
		reportResponse.setAmount(report.getAmount());
		reportResponse.setDateTime(report.getDateTime());
		reportResponse.setResponse("00");
		reportResponse.setToken(report.getToken());
		reportResponse.setTransactionType(report.getTransactionType());
		reportResponse.setVendorReference(report.getVendorReference());
		reportResponse.setBillAccount(payment.getBillAccount());
		return reportResponse;	
	}
	
	public ReportResponse mapReportException() {
		reportResponse.setAmount("");
		reportResponse.setDateTime("");
		reportResponse.setResponse("96");
		reportResponse.setToken("");
		reportResponse.setTransactionType("");
		reportResponse.setVendorReference("");
		return reportResponse;	
	}
	
	public PaymentResponse mapPayment(Payment payment) {
		
		PowertelReport report = reportService.findByVendorReference(payment.getId().toString());
		String token = report.getToken();
		paymentResponse.setBillAccount(payment.getBillAccount());
		paymentResponse.setBillAmount(payment.getBillAmount());
		paymentResponse.setChannel(payment.getChannel());
		paymentResponse.setDateSubmitted(payment.getDateSubmitted());
		paymentResponse.setId(payment.getId());
		paymentResponse.setParticipantId(payment.getParticipantId());
		paymentResponse.setParticipantReference(payment.getParticipantReference());
		paymentResponse.setPayerMsisdn(payment.getPayerMsisdn());
		paymentResponse.setPortalDescription(payment.getPortalDescription());
		paymentResponse.setPortalResponse(payment.getPortalResponse());
		paymentResponse.setProvider(payment.getProvider());
		paymentResponse.setProviderProduct(payment.getProviderProduct());
		paymentResponse.setProviderResponse(payment.getProviderResponse());
		paymentResponse.setResponse("00");
		paymentResponse.setState(payment.getState());
		paymentResponse.setSuccess(payment.getSuccess());
		paymentResponse.setToken(token);
		paymentResponse.setType(payment.getType());
		paymentResponse.setProviderReference(payment.getProviderReference());
		return paymentResponse;
		
	}
	
	
	public PaymentResponse mapPaymentException() {
		paymentResponse.setBillAccount("");
		paymentResponse.setBillAmount("");
		paymentResponse.setChannel("");
		paymentResponse.setDateSubmitted("");
		paymentResponse.setId(1L);
		paymentResponse.setParticipantId("");
		paymentResponse.setParticipantReference("");
		paymentResponse.setPayerMsisdn("");
		paymentResponse.setPortalDescription("");
		paymentResponse.setPortalResponse("");
		paymentResponse.setProvider("");
		paymentResponse.setProviderProduct("");
		paymentResponse.setProviderResponse("");
		paymentResponse.setResponse("96");
		paymentResponse.setState("");
		paymentResponse.setSuccess("");
		paymentResponse.setToken("");
		paymentResponse.setType("");
		paymentResponse.setProviderReference("");

		return paymentResponse;
	}
}
